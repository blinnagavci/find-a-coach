export default {

    // add request.
    addRequest(state, payload){
        state.requests.unshift(payload);
    },

    // update requests list.
    setRequests(state, payload){
        state.requests = payload;
    }
}
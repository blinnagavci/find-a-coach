export default {

    // add new request.
    async addRequest(context, payload) {
        const request = {
            id: Math.floor(Math.random() * 1000000000),
            email: payload.email,
            message: payload.message
        };

        // save request to firebase.
        const response = await fetch('https://find-a-coach-815b7-default-rtdb.firebaseio.com/requests/' + payload.coachID + '.json', {
            method: 'POST',
            body: JSON.stringify(request)
        });

        const responseData = await response.json();

        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to send request!');
            throw error; // if response is not ok, throw error.
        }

        request.coachID = payload.coachID; // add coach id to request object.
        

        context.commit('addRequest', request); // save request to vuex state.
    },

    // load all requests.
    async loadRequests(context) {
        const coachId = context.rootGetters.getUserID; // get current user id.
        const response = await fetch('https://find-a-coach-815b7-default-rtdb.firebaseio.com/requests/' + coachId + '.json'); // get all requests from firebase.

        const responseData = await response.json();

        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch requests!');
            throw error; // if response is not ok, throw error.
        }

        const tempRequests = []; // save response data to a temporary array.

        for (const key in responseData) {
            const request = {
                id: responseData[key].id,
                coachID: coachId,
                email: responseData[key].email,
                message: responseData[key].message
            }

            tempRequests.push(request);
        }

        context.commit('setRequests', tempRequests); // update requests to vuex store.
    }
}
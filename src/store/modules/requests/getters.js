export default {

    // returns all requests.
    allRequests(state) {
        return state.requests;
    },

    // returns all requests of the current coach.
    coachRequests(state, _, _2, rootGetters) {
        const coachID = rootGetters.getUserID;
        return state.requests.filter(request => request.coachID == coachID);
    },

    // returns true if current coach has requests.
    hasRequests(_, getters) {
        return getters.coachRequests && getters.coachRequests.length > 0;
    }
}
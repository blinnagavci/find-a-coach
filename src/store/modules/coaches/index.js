import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
    namespaced: true,
    state: {
        lastFetch: null,
        selectedCoach: null, // selected coach for coach details page.
        coaches: [] // coaches list.
    },
    mutations,
    actions,
    getters
}
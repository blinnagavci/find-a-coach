export default {
    // add new coach to the coaches list.
    addCoach(state, payload) {
        state.coaches.unshift(payload);
    },

    // update coaches list.
    setCoaches(state, payload) {
        state.coaches = payload;
    },

    // update selected coach for coach details page.
    setSelectedCoach(state, payload){
        state.selectedCoach = payload;
    },

    // update last fetch timestamp in order to cache coaches.
    setFetchTimestamp(state) {
        state.lastFetch = new Date().getTime(); // set lastFetch variable to current time
    }
};
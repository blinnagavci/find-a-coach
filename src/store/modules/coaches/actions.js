export default {

    // register new coach.
    async addCoach(context, payload) {
        const userID = context.rootGetters.getUserID;

        const coach = {
            firstName: payload.firstName,
            lastName: payload.lastName,
            description: payload.description,
            areas: payload.areas,
            hourlyRate: payload.hourlyRate
        }

        // save coach to firebase.
        const response = await fetch('https://find-a-coach-815b7-default-rtdb.firebaseio.com/coaches/' + userID + '.json', {
            method: 'PUT',
            body: JSON.stringify(coach)
        });

        const responseData = await response.json();

        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to send data!');
            throw error; // if response is not ok, throw error.
        }

        // save data to vuex state.
        context.commit('addCoach', {
            ...coach,
            id: userID
        });
    },

    // load selected coach for coach details page.
    async loadSelectedCoach(context, payload) {

        const response = await fetch('https://find-a-coach-815b7-default-rtdb.firebaseio.com/coaches/' + payload.id + '.json'); // get selected coach of the coach details page.

        const responseData = await response.json();

        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch data!');
            throw error;  // if response is not ok, throw error.
        }

        const tempCoach = responseData;

        context.commit('setSelectedCoach', tempCoach); // update selected coach in vuex.
    },

    // load all coaches.
    async loadCoaches(context, payload) {

        // if there is less than a minute from last fetch and user does not use force refresh, break code.
        if (!context.getters.shouldUpdate() && !payload.forceRefresh) {
            return;
        }

        const response = await fetch('https://find-a-coach-815b7-default-rtdb.firebaseio.com/coaches.json'); // get all coaches from firebase.

        const responseData = await response.json();

        if (!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch data!');
            throw error;  // if response is not ok, throw error.
        }

        const tempCoaches = []; // save response data to a temporary array.

        for (const key in responseData) {
            const coach = {
                id: parseInt(key),
                firstName: responseData[key].firstName,
                lastName: responseData[key].lastName,
                description: responseData[key].description,
                areas: responseData[key].areas,
                hourlyRate: responseData[key].hourlyRate
            };

            tempCoaches.unshift(coach);
        }

        context.commit('setCoaches', tempCoaches); // update coaches list.
        context.commit('setFetchTimestamp'); // update last fetch timestamp.
    }
}
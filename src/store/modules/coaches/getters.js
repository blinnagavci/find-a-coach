export default {
    
    // returns all coaches.
    allCoaches(state) {
        return state.coaches;
    },

    // returns true if there is at least one coach registred.
    hasCoaches(state) {
        return state.coaches && state.coaches.length > 0;
    },

    // returns true if the current user is a coach.
    isCoach(_, getters, _2, rootGetters) {
        const coaches = getters.allCoaches;
        const userID = rootGetters.getUserID;

        return coaches.some(coach => coach.id === userID);
    },
    
    // get Selected Coach for coach details page.
    getSelectedCoach(state){
        return state.selectedCoach;
    },

    // method-style access getter, so the result is not cached into Vue reactivity system.
    // returns true if the coaches list should be updated.
    shouldUpdate: (state) => () => {
        const lastFetch = state.lastFetch; // get the last time coaches list was updated.

        if (!lastFetch) {
            return true;  // should update if lastFetch variable is null.
        }

        const currentTimeStamp = new Date().getTime();
        return (currentTimeStamp - lastFetch) / 1000 > 60; // returns true if last fetched was more than a minute ago.
    }
}
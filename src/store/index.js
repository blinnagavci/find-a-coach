import Vue from 'vue'
import Vuex from 'vuex'
import coachesModule from './modules/coaches/index.js'
import requestsModule from './modules/requests/index.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userID: 2 // current user id.
  },
  
  getters: {
    // return current user id.
    getUserID(state){
      return state.userID
    }
  },

  modules: {
    coaches: coachesModule,
    requests: requestsModule
  }
})

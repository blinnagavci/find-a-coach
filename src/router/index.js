import Vue from 'vue'
import VueRouter from 'vue-router'
import CoachesList from '@/views/coaches/CoachesList.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/coaches'
  },
  {
    path: '/coaches',
    name: 'Coaches List',
    component: CoachesList,
  },
  {
    path: '/coaches/:id',
    name: 'Coach Details',
    component: () => import('@/views/coaches/CoachDetails.vue'),
    children: [
      {
        path: 'contact', // /coaches/c1/contact
        name: 'Contact Coach',
        component: () => import('@/views/requests/ContactCoach.vue')
        
      }
    ]
  },
  {
    path: '/register',
    name: 'Coach Registration',
    component: () => import('@/views/coaches/CoachRegistration.vue')
  },
  {
    path: '/requests',
    name: 'Requests List',
    component: () => import('@/views/requests/RequestsList.vue')
  },
  {
    path: '*',
    name: 'Not Found',
    component: () => import('@/views/NotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

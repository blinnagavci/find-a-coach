import Vue from 'vue'
import App from './App.vue'

// IMPORT ROUTING
import router from './router'

// IMPORT VUEX
import store from './store'

// IMPORT VALIDATION
import Vuelidate from 'vuelidate'

// IMPORT BOOTSTRAP
import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'

// IMPORT GLOBAL COMPONENTS
import BaseButton from '@/components/ui/BaseButton.vue'
import BaseLink from '@/components/ui/BaseLink.vue'
import BaseBadge from '@/components/ui/BaseBadge.vue'
import BaseSpinner from '@/components/ui/BaseSpinner.vue'
import BaseModal from '@/components/ui/BaseModal.vue'

Vue.config.productionTip = false

Vue.use(Vuelidate);

// GLOBAL COMPONENTS
Vue.component('base-button', BaseButton);
Vue.component('base-link', BaseLink);
Vue.component('base-badge', BaseBadge);
Vue.component('base-spinner', BaseSpinner);
Vue.component('base-modal', BaseModal);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
